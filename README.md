Basic framework for energy reconstruction in the AHCAL using a 
neural network, global and local software compensation. 

Feature generation and event selection
are included.

Input: Root files after reconstruction. The root files are transformed
to pandas data frames. First global features can be calculated. The
second step is an event selection. Finally, different models can be trained
on the data. The three steps are implemented as separate modules. In between
steps, the dataframes are saved as pickle files.

run with python 3.5.4 64-bit "conda setup"
pyroot needed (v6.14.04 of root used)

### dependencies:
numpy
pandas
tqdm
matplotlib
seaborn
keras
tensorflow
sklearn

root_pandas (https://github.com/scikit-hep/root_pandas)

### Overview

utils.py provides utility functions that are needed in several places

makeFeatures.py reads in root files and generates features. 
It can reduce the number of layers and cut on the maximum hit time.
Outputs ROOT files and/or pickle files

eventSelection.py does the event selection. Reads in pickle files. 
ROOT files are currently not supported because of a problem with root_pandas
not storing arrays in cells correctly in ROOT files.

trainModel.py Training of models. Reads in pickle files.

#### Example use

1. Generate global observables

python src/makeFeatures.py --input rawInputs.txt --outputPath makeFeaturesOut/ -p -r

rawInputs.txt is a file which specifies the input files, one in each line in the format:

< energy >,< path to root file >

The output will be saved as root and pickle files

2. Event selection

python src/eventSelection.py --input eventSelectionInputs.txt --outputPath eventSelectionOut/ -p -r

eventSelectionInputs.txt is a file which specifies the input files (output of makeFeatures.py), one in each line in the format:

< energy >,< path to pickle file >

3. Training of Models

python src/trainModel.py --input inputs.txt --outputPath ./data/200826/03trainModel/ -p -r

inputs.txt is a file which specifies the input files (output of eventSelection.py), one in each line in the format:

< energy >,< path to pickle file >

#### Models

globalSC.py Global Software compensation using up to two observables

localSC.py Local Software compensation. Possibility to use timing.

kerasNN.py Neural Network model.

#### Misc
Author: Christian Graf (cgraf \att mpp.mpg.de)
Date: May 2021