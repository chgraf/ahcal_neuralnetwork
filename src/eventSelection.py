# Preprocessing and event selection
# Standalone module

import ROOT
from root_pandas import read_root

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns

from scipy import optimize
from tqdm import tqdm

import argparse
import csv

from math import *

import os
import sys
sys.path.append("/Users/christiangraf/anaconda/envs/pyRoot/lib/python3.5/site-packages/")

import utils

#pandas options
pd.options.mode.chained_assignment = None  # default='warn'

#infrastructure
parser = argparse.ArgumentParser()
parser.add_argument("--input", "-i", help="txt file with a list of input files in the format: TrueEnergy,fileName")
parser.add_argument("--outputPath", "-o", help="set the output path")
parser.add_argument("--outputPickle", "-p", help="output Pickle files?", action='store_true')
parser.add_argument("--outputRoot", "-r", help="output Root files?", action='store_true')
args = parser.parse_args()

#global variables
_nLayer = 60
_hardwareConnection = dict()

_energies = []
_fileNames = []

#options
_minShowerStart = 1
_maxShowerStart = 10

_minEnergy = 2.5
_maxEnergy = 2.5


def selectShowerStart(df, minShowerStart, maxShowerStart):
    df = df[df["showerStart"] >= minShowerStart]
    df = df[df["showerStart"] <= maxShowerStart]
    return df

def selectEnergyRange(df, E, minEnergy, maxEnergy):
    df = df[df["stdReco"] >= E - minEnergy * np.sqrt(E)]
    df = df[df["stdReco"] <= E + maxEnergy * np.sqrt(E)]
    return df


def main():
    print("Starting event selection...\n")
    _hardwareConnection = utils.read_hardwareConnection("MappingIJK_mcc.txt")

    if args.input:
        print("\nList of input files in: ", args.input)
        with open(args.input) as f:

            for line in csv.reader(f, delimiter=','):
                _energies.append(float(line[0]))
                _fileNames.append(str(line[1]).strip())

        print("Reading " + str(len(_fileNames)) + " input files.")
    else:
        print("No input file found. Set input file with --input.")

    df = pd.DataFrame()

    for fn, E in tqdm(zip(_fileNames, _energies)):
        tqdm.write("\nReading energy " + str(E) + " GeV from file: " + str(fn))

        thisDf = utils.readPickleFile(fn, E)

        _lenBeforeShowerStartCut = []
        _lenAfterShowerStartCut = []
        _lenBeforeEnergyCut = []
        _lenAfterEnergyCut = []

        _lenBeforeShowerStartCut.append(len(thisDf))
        thisDf = selectShowerStart(thisDf, _minShowerStart, _maxShowerStart)
        _lenAfterShowerStartCut.append(len(thisDf))

        df = pd.concat([df, thisDf], copy=False)
        del thisDf

    Mip2GeV = utils.getMip2GeV(df, "ahc_energySum", "beamEnergy", energyCut=0.1)
    df = utils.updateStdReco(df, Mip2GeV, mipField="ahc_energySum", stdRecoField="stdReco")

    for fn, E in tqdm(zip(_fileNames, _energies)):
        myDf = df[df["beamEnergy"] == E]

        _lenBeforeEnergyCut.append(len(myDf))
        #myDf = selectEnergyRange(myDf, E, _minEnergy, _maxEnergy)
        #_lenAfterEnergyCut.append(len(myDf))

        # write output
        outputPath = "./data/60LayersScan/02eventSelection/"
        addName = "_selection"
        if args.outputPath:
            outputPath = args.outputPath
        baseName = os.path.splitext(os.path.basename(fn))[0]
        outputFile = outputPath+baseName+addName

        if args.outputRoot:
            print("saving file to " + outputFile + ".root")
            myDf.to_root(outputFile+".root", key="bigtree")

        if args.outputPickle:
            print("saving file to " + outputFile + ".pickle")
            myDf.to_pickle(outputFile+".pickle")
    
        
if __name__ == "__main__":
    main()