# Model for global Software Compensation
# Up to two global features can be used
#
# Author: Christian Graf (cgraf att mpp.mpg.de)
# Date: May 2021

import numpy as np
import pandas as pd

from scipy import optimize

import json

import utils


class globalSC:

    features = []
    results = 0

    # Energy dependent global software compensation gives variability in the weights for different energies
    isEnergyDependent = False 

    #use two linear features
    def funFeatures(self, params, df, features):
        assert len(features) == 2
        assert len(params) == 4
        f0 = df[features[0]]
        f1 = df[features[1]]
        return params[0] + params[1]*f0 + params[2]*f1 + params[3]*f0*f1

    def loss_funFeatures(self, params, df, sfeature, timeCorrected=False):
        s = 0.

        energySum = df["ahc_energySum"]
        beamEnergy = df["beamEnergy"]
        
        assert len(sfeature) == 2

        s = np.sum(np.power((energySum*self.funFeatures(params, df, sfeature) - beamEnergy)/np.sqrt(beamEnergy),2))

        return s

    # single feature function
    def funFeature(self, params, df, feature):
        assert len(feature) == 1
        assert len(params) == 3
        f0 = df[feature[0]]
        return params[0] + params[1]*f0 + params[2]*f0*f0

    def loss_funFeature(self, params, df, sfeature, timeCorrected=False):
        s = 0.
        
        energySum = df["ahc_energySum"]
        beamEnergy = df["beamEnergy"]

        assert len(sfeature) == 1

        s = np.sum(np.power((energySum*self.funFeature(params, df, sfeature) - beamEnergy)/np.sqrt(beamEnergy),2))

        return np.power(s,2)

    def trainModel(self, df_train):
        iterations = 2000

        res=0

        if not self.isEnergyDependent:

            if len(self.features) == 1:
                initial_guess = [0.03, 1, 1]
                res = optimize.minimize(self.loss_funFeature, x0=initial_guess, args=(df_train, self.features), method="Nelder-Mead", options={"maxiter": iterations, "maxfev": iterations} )
            elif len(self.features) == 2: 
                initial_guess = [0.03, 1, 1, 1]
                res = optimize.minimize(self.loss_funFeatures, x0=initial_guess, args=(df_train, self.features), method="Nelder-Mead", options={"maxiter": iterations, "maxfev": iterations} )
        
        else:

            energies = np.unique(df_train["beamEnergy"])
            energySums = []

            params = dict()

            for E in energies:
                myDf = df_train[df_train["beamEnergy"] == E]
                energySums.append(utils.mean90(myDf["ahc_energySum"]))

                if len(self.features) == 1:
                    initial_guess = [0.03, 1, 1]
                    res = optimize.minimize(self.loss_funFeature, x0=initial_guess, args=(myDf, self.features), method="Nelder-Mead", options={"maxiter": iterations, "maxfev": iterations} )
                elif len(self.features) == 2: 
                    initial_guess = [0.03, 1, 1, 1]
                    res = optimize.minimize(self.loss_funFeatures, x0=initial_guess, args=(myDf, self.features), method="Nelder-Mead", options={"maxiter": iterations, "maxfev": iterations} )

                params[E] = res.x
                print()
                print("Energy: ", E)
                print(params[E])

            # fitting the parameters
            n = len(params[energies[0]]) # number of parameters
            weightParams = []
            for i in range(0,n):
                p = []
                for E in energies:
                    p.append(params[E][i])
                
                quad = lambda x, a, b, c: a + b*x + c*x*x
                res = optimize.curve_fit(f=quad, xdata=energySums, ydata=p, p0=[0.0,0.0,0.0])
                weightParams.append(res[0])
                print()
                print(i)
                print(res[0])

            res = weightParams

        self.results = res

        print(res)

        return res

    def testModel(self, df_test):
        energySum = df_test["ahc_energySum"]

        if not self.isEnergyDependent:

            if len(self.features) == 1:
                prediction = energySum*self.funFeature(self.results.x, df_test, self.features)
            elif len(self.features) == 2: 
                prediction = energySum*self.funFeatures(self.results.x, df_test, self.features)

        else:
            quad = lambda x, a, b, c: a + b*x + c*x*x


            if len(self.features) == 1:
                b0 = quad(energySum, self.results[0][0], self.results[0][1], self.results[0][2])
                b1 = quad(energySum, self.results[1][0], self.results[1][1], self.results[1][2])
                b2 = quad(energySum, self.results[2][0], self.results[2][1], self.results[2][2])

                x = df_test[self.features[0]]

                prediction = energySum*(b0 + b1*x + b2*x*x)

                print("B0")
                print(b0)
                print("B1")
                print(b1)
                print("B2")
                print(b2)

                print("feature")
                print(x)
                print("energySum")
                print(energySum)

                print("Prediction")
                print(prediction)

            elif len(self.features) == 2: 
                b0 = quad(energySum, self.results[0][0], self.results[0][1], self.results[0][2])
                b1 = quad(energySum, self.results[1][0], self.results[1][1], self.results[1][2])
                b2 = quad(energySum, self.results[2][0], self.results[2][1], self.results[2][2])
                b3 = quad(energySum, self.results[3][0], self.results[3][1], self.results[3][2])

                x1 = df_test[self.features[0]]
                x2 = df_test[self.features[1]]

                prediction = energySum*(b0 + b1*x1 + b2*x2 + b3*x1*x2)

        return prediction

    def writeModel(self, fn):
        res = self.results
        #del res.specs["args"]["func"]

        with open('fn'+json, 'wb') as f:
            json.dump(res.tolist(), f, separators=(',', ':'))

    def __init__(self, features, isEnergyDependent):
        self.features = features
        self.isEnergyDependent = isEnergyDependent