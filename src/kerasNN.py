# Implementation of a simple neural network in keras 
# estimating the energy of a particle shower
# Input: global shower observables
# Target: dE = E_reco / E_true
#
# Author: Christian Graf (cgraf att mpp.mpg.de)
# Date: May 2021

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns

import keras
from keras.models import Sequential, Model
from keras.models import model_from_json
from keras.layers import Input, Dense, Dropout, Flatten, Activation, Lambda
from keras.constraints import maxnorm
from keras.optimizers import Adam, SGD
from keras.layers import Activation
from keras.layers.normalization import BatchNormalization
from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras import layers
from keras import models

from keras import backend as K
import tensorflow as tf

#print("Keras Version:", keras.__version__)
#print("Tensorflow Version:",tf.__version__)

class kerasNN:

    features = []
    model = 0
    model_history = 0

    tag = ""

    epochs = 25
    lossFunction = "mean_squared_error"
    arch=[128,64]           # Architecture: list of number of neurons in each layer
    dropout=[0.25,0.2]      # Values for potential dropout layers


    # Building the network
    def createModel(self):
        input_shape=(len(self.features),)

        model = Sequential()

        print("\nCreating NN with architecture: ", self.arch)

        n0 = int(self.arch[0])
        model.add(Dense(n0, 
                        input_shape=input_shape,
                        activation='relu',
                        kernel_initializer="he_normal"
                ))
        d0 = float(self.dropout[0])/100.
        model.add(Dropout(d0))

        for n,d in zip(self.arch[1:], self.dropout[1:]):
            n = int(n)
            model.add(Dense(n,
                            activation='relu',
                            kernel_initializer="he_normal"
                        ))
            model.add(Dropout(0.15))
        
        model.add(Dense(1, activation='linear'))
    
        # Print model summary
        model.summary()
        
        # Compile neural network
        model.compile(optimizer="adam", # using adam optimizer
                    loss=self.lossFunction,
                    metrics=['mae']
                    )

        self.model = model
        return model  

    # Train model on training data set
    def trainModel(self, df_train):
        fFeatures = np.array(df_train[self.features]).reshape(-1,len(self.features))
        Y = df_train["dE"]

        self.model_history = self.model.fit(fFeatures, Y,
                                batch_size=128,
                                epochs=self.epochs,
                                validation_split=0.20,
                                shuffle=True
                                )

    # Plot training & validation loss values
    def plotModelHistory(self):
        plt.plot(self.model_history.history['loss'])
        plt.plot(self.model_history.history['val_loss'])
        plt.title('Model loss')
        plt.ylabel('Loss')
        plt.xlabel('Epoch')
        plt.legend(['Train', 'Test'], loc='upper right')
        max_lim = np.max(self.model_history.history['val_loss'])*1.0
        min_lim = np.min(self.model_history.history['loss'])*0.7
        plt.ylim(min_lim,max_lim)
        plt.grid(True)
        plt.savefig("plots/modelLoss_"+self.tag+".pdf")


    # generate predictions on test data set
    def testModel(self, df_test):
        prediction = self.model.predict(np.array(df_test[self.features]).reshape(-1,len(self.features))).flatten()

        return prediction

    # Write the model to json
    def writeModel(self, fn):
        # serialize model to JSON
        model_json = self.model.to_json()
        with open(fn+".json", "w") as json_file:
            json_file.write(model_json)

        # serialize weights to HDF5
        self.model.save_weights(fn+".h5")
        print("Saved model to disk:", fn)

    # features is a list of column names of the input features in the data frame
    def __init__(self, features, tag="", epochs=25, loss="mean_squared_error", arch=[128,64], drop=[0.25,0.2]):
        print("\nInit NN")
        print("loss function:", loss)
        print("architecture:", arch)
        print("dropout:", drop)
        print("epochs:", epochs)

        if(len(arch) != len(drop)):
            print("\nERROR, architecture and dropout length do not match!")
            print("Architecture:",arch)
            print("Dropout:",drop)
            assert len(arch) == len(drop)

        self.features = features
        self.epochs = epochs
        self.lossFunction = loss
        self.arch = arch
        self.dropout = drop
        self.tag = tag
