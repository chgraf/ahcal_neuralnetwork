# Model for local Software Compensation
# Using stochastic gradient descent for optimization
# Possibility to use time bins 
#
# Author: Christian Graf (cgraf att mpp.mpg.de)
# Date: May 2021

import numpy as np
import pandas as pd

from scipy import optimize

import json
import math

class localSC:

    eBins = []
    tBins = []
    mip2GeV = 0
    results = []

    def calculateBins(self, hits, bins):
        return np.digitize(hits, bins, right=True)

    def generateHitArray(self, df, eCol="ahc_hitEnergy", tCol=None, nMax=0):
        eHits = [] # energy hits
        tHits = [] # time hits

        for idx, row in df.iterrows():
            e = row[eCol]
            eHits.append(np.hstack([e, np.zeros(nMax-e.shape[0])]))

            if tCol != None:
                t = row[tCol]
                tHits.append(np.hstack([t, np.zeros(nMax-t.shape[0])]))

        eHits = np.stack(eHits, axis=0)
        eHitsBinned = self.calculateBins(eHits, self.eBins) # energyHits binned with respect to eBins
        tHitsBinned = []                                    # timeHits binned with respect to tBins

        if tCol != None:
            tHits = np.stack(tHits, axis=0)
            tHitsBinned = self.calculateBins(tHits, self.tBins)

        # energy hits binned with respect to energy and time
        eHitsBinned_time = (eHitsBinned + (len(self.eBins)-1)*tHitsBinned).astype(int)

        return eHits, eHitsBinned, eHitsBinned_time

    # quadratic function
    def quad(self, x, a,b,c):
        return a+np.multiply(b,x)+np.multiply(c,np.power(x,2))

    def getWeights(self, x, weights, beamEnergy):
        a = np.take(weights, 0+(x-1)*3)
        b = np.take(weights, 1+(x-1)*3)
        c = np.take(weights, 2+(x-1)*3)
        return self.quad(beamEnergy[:,None], a,b,c)

    # Calculating gradient of loss function
    def grad(self, w, hitEnergiesBinned, hitEnergies, beamEnergies):
        R = np.sum(self.getWeights(hitEnergiesBinned, w, beamEnergies)*hitEnergies*self.mip2GeV, axis=1) - beamEnergies
        K = 2 / (0.55*beamEnergies)
        G = []
        for i in range(math.floor((len(w)+1)/3)):
            S = np.sum((hitEnergiesBinned == i+1)*hitEnergies*self.mip2GeV, axis=1)
            G.append(np.mean(R*K*S*np.power(beamEnergies,0)))
            G.append(np.mean(R*K*S*np.power(beamEnergies,1))) #+ 2*w[i+1]*_beta)
            G.append(np.mean(R*K*S*np.power(beamEnergies,2))) #+ 2*w[i+1]*_beta*_beta)
        return np.array(G)

    # Stochastic gradient descent implementation
    def SGD(self, fun, x0, args=(), maxfev=None, alpha=0.9, eta=0.1,
            maxiter=100000, tol=1e-8, batch_size=1024, callback=None, **options):

        hitEnergiesBinned, hitEnergies, beamEnergies = args

        G = np.zeros(len(x0))
        P = np.zeros(len(x0))
        for i in range(len(x0)):
            P[i] = np.power(500,i%3)

        x0 = np.array(x0)
        bestx = x0
        bestf = fun(x0, hitEnergiesBinned, hitEnergies, beamEnergies)

        funcalls = 1
        niter = 0
        improved = True
        stop = False
        success = True
        
        eps = 1e-8
        
        lastLoss = 999
        loss = 99
        
        nCheck = 250
            
        while improved and not stop and niter < maxiter:

            idx = np.random.choice(beamEnergies.shape[0], batch_size).astype(int)
            hitEnergiesBinned_batch = hitEnergiesBinned[idx]
            hitEnergies_batch = hitEnergies[idx]
            beamEnergies_batch = beamEnergies[idx]
        
            niter += 1
            # the next 2 lines are gradient descent
            g = self.grad(bestx, hitEnergiesBinned_batch, hitEnergies_batch, beamEnergies_batch)
            if(niter % nCheck == 0):
                g = self.grad(bestx, hitEnergiesBinned, hitEnergies, beamEnergies)
    
            G = alpha*G + (1-alpha)*g*g
            step = eta/np.sqrt(G+eps)*g

            if(niter % nCheck == 0):
                print("----", funcalls ,"----")
                print(bestx)
                print(step)
            
            bestx = bestx - step/P
            
            
            bestf = fun(bestx, hitEnergiesBinned_batch, hitEnergies_batch, beamEnergies_batch)
            
            if(niter % nCheck == 0):
                bestf = fun(bestx, hitEnergiesBinned, hitEnergies, beamEnergies)
                lastLoss = loss
                loss = bestf

                print(bestf)
                print("---------")
                
            funcalls += 1

            if lastLoss - loss < tol:
                improved = False
            if callback is not None:
                callback(bestx)
            if maxfev is not None and funcalls >= maxfev:
                stop = True
                break
            if not improved:
                stop = True
                success = True
                break
                
        return optimize.OptimizeResult(fun=bestf, x=bestx, nit=niter,
                                nfev=funcalls, success=success)


    def chi2localSC_np_quad(self, weights, hitEnergyBinned, hitEnergy, beamEnergy):
        r = np.mean(np.power(np.sum(self.getWeights(hitEnergyBinned, weights, beamEnergy)*hitEnergy, axis=1)*self.mip2GeV-beamEnergy, 2)/(0.55*0.55*beamEnergy))  
        
        return r

    # Train model on training data set
    def trainModel(self, df_train, hitEnergies_train, hitEnergiesBinned_train=None, hitEnergiesBinnedTime_train=None, energyAndTime=False):
        iterations = 5000
        myDf = df_train

        res = None
        if not energyAndTime:
            initial_guess = np.tile([1.0, 0.0, 0.0], len(self.eBins)-1)
            res = optimize.minimize(self.chi2localSC_np_quad, x0=initial_guess, args=(hitEnergiesBinned_train, hitEnergies_train, myDf["beamEnergy"].values), method=self.SGD, options={"maxiter": iterations, "maxfev": iterations, "alpha": 0.9, "eta": 5e-3, "tol": 1e-5, "batch_size": 256} )
        else:
            initial_guess = np.tile([1.0, 0.0, 0.0], (len(self.eBins)-1)*2)
            res = optimize.minimize(self.chi2localSC_np_quad, x0=initial_guess, args=(hitEnergiesBinnedTime_train, hitEnergies_train, myDf["beamEnergy"].values), method=self.SGD, options={"maxiter": iterations, "maxfev": iterations, "alpha": 0.9, "eta": 5e-3, "tol": 1e-5, "batch_size": 256} )

        print(res)
        self.results = res

        return res.x

    # Generate model predictions on test data set
    def testModel(self, df_test, weights, hitEnergies, hitEnergiesBinned=None, hitEnergiesBinnedTime=None, energyAndTime=False):
        prediction = 0
        stdReco = df_test["stdReco"].values

        if not energyAndTime:
            prediction = np.sum(self.getWeights(hitEnergiesBinned, weights, stdReco)*hitEnergies, axis=1)*self.mip2GeV
        else:
            prediction = np.sum(self.getWeights(hitEnergiesBinnedTime, weights, stdReco)*hitEnergies, axis=1)*self.mip2GeV

        return prediction

    def writeModel(self, fn):
        res = self.results
        #del res.specs["args"]["func"]

        res = {k: v.tolist() if isinstance(v, np.ndarray) else v for k, v in res.items()}
        with open(fn+".json", 'w', encoding="utf8") as f:
            json.dump(res, f, separators=(',', ':'))

    def __init__(self, eBins, tBins, mip2GeV):
        self.eBins = eBins
        self.tBins = tBins
        self.mip2GeV = mip2GeV