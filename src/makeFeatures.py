# Computing global features
# Can be used as a module or standalone
#
# Date: May 2021

import ROOT
from root_pandas import read_root

import numpy as np
import pandas as pd

from tqdm import tqdm

import argparse
import csv

from math import *

import os
import sys
sys.path.append("/Users/christiangraf/anaconda/envs/pyRoot/lib/python3.5/site-packages/")

import utils

#pandas options
pd.options.mode.chained_assignment = None  # default='warn'

#infrastructure
parser = argparse.ArgumentParser()
parser.add_argument("--input", "-i", help="txt file with a list of input files in the format: TrueEnergy,fileName")
parser.add_argument("--outputPath", "-o", help="set the output path")
parser.add_argument("--outputPickle", "-p", help="output Pickle files?", action='store_true')
parser.add_argument("--outputRoot", "-r", help="output Root files?", action='store_true')
parser.add_argument("--maxLayer", "-L", help="reduce the number of Layers")
parser.add_argument("--maxHitTime", "-T", help="cut away too late Hits")
args = parser.parse_args()

#global variables
_nLayer = 60
_hardwareConnection = dict()

# Preparing data for local software compensation
_localSC = False

# Preparing data for Conv NN
_convNN = True

#options
_timeShift = -3.5

#Reading the ROOT files into pandas dataframes    
#def readRootFile(myFile, E):
    #df = read_root(myFile, columns=['ahc_nHits', 'ahc_energySum', 'ahc_radius', 'ahc_cogZ', 'ahc_energyPerLayer', 'ahc_nHitsPerLayer', 'ahc_hitEnergy', 'ahc_hitRadius', 'ahc_hitTime', 'ahc_hitK'])
    #df["beamEnergy"] = E
    #return df

def shiftHitTime(df, timeShift=-3.5):
    #Shifting MC hit times by 3.5ns
    df["ahc_hitTime"] = df["ahc_hitTime"].apply(lambda x: np.add(x,timeShift))
    return df

def removeLateHits(df, threshold):
    for index, row in df.iterrows():
        mask = row["ahc_hitTime"] < threshold
        df.at[index, "ahc_hitTime"] = row["ahc_hitTime"][mask]
        df.at[index, "ahc_hitEnergy"] = row["ahc_hitEnergy"][mask]
        df.at[index, "ahc_hitRadius"] = row["ahc_hitRadius"][mask]
        df.at[index, "ahc_hitK"] = row["ahc_hitK"][mask]

    return df    

def reduceLayers(df, maxLayer):
    for index, row in df.iterrows():
        mask = row["ahc_hitK"] <= maxLayer
        df.at[index, "ahc_hitTime"] = row["ahc_hitTime"][mask]
        df.at[index, "ahc_hitEnergy"] = row["ahc_hitEnergy"][mask]
        df.at[index, "ahc_hitRadius"] = row["ahc_hitRadius"][mask]
        df.at[index, "ahc_hitK"] = row["ahc_hitK"][mask]
    
    return df

def recalculateVariables(df):
    for index, row in df.iterrows():
        df.at[index, "ahc_nHits"]  = len(row["ahc_hitTime"])
        df.at[index, "ahc_energySum"]  = np.sum(row["ahc_hitEnergy"])
        df.at[index, "ahc_radius"] = np.mean(df.at[index, "ahc_hitRadius"])
        
        df.at[index, "ahc_nHitsPerLayer"] = [np.sum(df.at[index, "ahc_hitK"] == k) for k in range(1,_nLayer+1)]
        df.at[index, "ahc_energyPerLayer"] = [np.sum(row["ahc_hitEnergy"][df.at[index, "ahc_hitK"] == k]) for k in range(1,_nLayer+1)]    

    return df

def calculateShowerStart(df):
    #Gives the definition of the shower start for energyPerLayer (1-3) and nHitsPerLayer (4-6)
    #For three consecutive layers
    showerStartParam = (3,5,8,2,3,4)
    showerStartParam = np.array([showerStartParam for x in df["ahc_nHits"]])

    nHitsPerLayer = np.concatenate(df["ahc_nHitsPerLayer"].values).reshape(len(df["ahc_nHitsPerLayer"].values),len(df["ahc_nHitsPerLayer"].values[0]))
    energyPerLayer = np.concatenate(df["ahc_energyPerLayer"].values).reshape(len(df["ahc_energyPerLayer"].values),len(df["ahc_energyPerLayer"].values[0]))
    #df["showerStart"] = np.argmax([(df["EPL_{}".format(k)] > showerStartParam[:,0]) & (df["NPL_{}".format(k)] > showerStartParam[:,3]) & (df["EPL_{}".format(k+1)] > showerStartParam[:,1]) & (df["NPL_{}".format(k+1)] > showerStartParam[:,4]) & (df["EPL_{}".format(k+2)] > showerStartParam[:,2]) & (df["NPL_{}".format(k+2)] > showerStartParam[:,5]) for k in range(0,_nLayer-2)], axis=0)
    df["showerStart_nHits"] = np.argmax([(nHitsPerLayer[:,k] > showerStartParam[:,3]) & (nHitsPerLayer[:,k+1] > showerStartParam[:,4]) & (nHitsPerLayer[:,k+2] > showerStartParam[:,5]) for k in range(0,_nLayer-2)], axis=0)
    df["showerStart_energy"] = np.argmax([(energyPerLayer[:,k] > showerStartParam[:,0]) & (energyPerLayer[:,k+1] > showerStartParam[:,1]) & (energyPerLayer[:,k+2] > showerStartParam[:,2]) for k in range(0,_nLayer-2)], axis=0)
    df["showerStart"] = np.maximum(df["showerStart_nHits"], df["showerStart_energy"])

    return df

def subtractShowerStart(df):
    nHitsPerLayer = np.concatenate(df["ahc_nHitsPerLayer"].values).reshape(len(df["ahc_nHitsPerLayer"].values),len(df["ahc_nHitsPerLayer"].values[0]))
    energyPerLayer = np.concatenate(df["ahc_energyPerLayer"].values).reshape(len(df["ahc_energyPerLayer"].values),len(df["ahc_energyPerLayer"].values[0]))
    df["energyInLastLayers"] = energyPerLayer[:,_nLayer-3]+energyPerLayer[:,_nLayer-2]+energyPerLayer[:,_nLayer-1]
    
    nHitsPerLayer = [np.roll(nHitsPerLayer[k], -df["showerStart"].iloc[k]) for k in range(0,len(df["showerStart"]))]
    energyPerLayer = [np.roll(energyPerLayer[k], -df["showerStart"].iloc[k]) for k in range(0,len(df["showerStart"]))]
    for k in range(0,len(df["showerStart"])):
        nHitsPerLayer[k][_nLayer-df["showerStart"].iloc[k]:,] = 0
        energyPerLayer[k][_nLayer-df["showerStart"].iloc[k]:,] = 0
    df["ahc_nHitsPerLayer"] = nHitsPerLayer
    df["ahc_energyPerLayer"] = energyPerLayer

    return df

def transformLayerData(df):
    nHitsPerLayer = np.concatenate(df["ahc_nHitsPerLayer"].values).reshape(len(df["ahc_nHitsPerLayer"].values),len(df["ahc_nHitsPerLayer"].values[0]))
    energyPerLayer = np.concatenate(df["ahc_energyPerLayer"].values).reshape(len(df["ahc_energyPerLayer"].values),len(df["ahc_energyPerLayer"].values[0]))

    for l in range(0,_nLayer):
        df["NPL_{}".format(l)] = np.array(nHitsPerLayer)[:,l]/df["ahc_nHits"]
        df["EPL_{}".format(l)] = np.array(energyPerLayer)[:,l]/df["ahc_energySum"]
        df["ENPL_{}".format(l)] = np.divide(np.array(energyPerLayer)[:,l], np.array(nHitsPerLayer)[:,l], out=np.zeros_like(np.array(energyPerLayer)[:,l]), where=np.array(nHitsPerLayer)[:,l]!=0)
        
    df = df.drop(columns=["ahc_nHitsPerLayer","ahc_energyPerLayer"])

    return df

def calculateNormCogZ(df):
    df["normCogZ"] = 0
    for k in range(0,_nLayer):
        df["normCogZ"] += df["EPL_{}".format(k)]*k

    return df

# The features to be calculated
def calculateVariables(df):
    if _convNN:
        df["eDepLateHits1"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 3) for i in range(0,len(df.index))]
        df["eDepLateHits2"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 10) for i in range(0,len(df.index))]
        df["eDepLateHits3"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 50) for i in range(0,len(df.index))]
        df["eDepLateHits4"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 75) for i in range(0,len(df.index))]

        df["eDepLateHits1"] = df["eDepLateHits1"] / df["ahc_energySum"]
        df["eDepLateHits2"] = df["eDepLateHits2"] / df["ahc_energySum"]
        df["eDepLateHits3"] = df["eDepLateHits3"] / df["ahc_energySum"]
        df["eDepLateHits4"] = df["eDepLateHits4"] / df["ahc_energySum"]

        df["fHighEnergyHits1"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 5)) / df["ahc_nHits"]
        df["fHighEnergyHits2"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 100)) / df["ahc_nHits"]
        df["fHighEnergyHits3"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 200)) / df["ahc_nHits"]

        df["meanEnergy"] = df["ahc_hitEnergy"].apply(np.mean)

        df["fHighEnergyHitsMean"] = [np.sum(df["ahc_hitEnergy"].values[i] > df["meanEnergy"].values[i]) / df["ahc_nHits"].values[i]for i in range(len(df["ahc_hitEnergy"]))]

        df["Cglobal"] = df["fHighEnergyHits1"] / df["fHighEnergyHitsMean"]       
        
        df["eDepNeutronElastic"] = df["eDepLateHits1"]-df["eDepLateHits3"]

        df["stdReco"] = df["ahc_energySum"]*0.0267     

    elif _localSC:
        df["eDepLateHits1"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 3) for i in range(0,len(df.index))]
        df["eDepLateHits2"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 10) for i in range(0,len(df.index))]
        df["eDepLateHits3"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 50) for i in range(0,len(df.index))]
        df["eDepLateHits4"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 75) for i in range(0,len(df.index))]

        df["eDepLateHits1"] = df["eDepLateHits1"] / df["ahc_energySum"]
        df["eDepLateHits2"] = df["eDepLateHits2"] / df["ahc_energySum"]
        df["eDepLateHits3"] = df["eDepLateHits3"] / df["ahc_energySum"]
        df["eDepLateHits4"] = df["eDepLateHits4"] / df["ahc_energySum"]

        df["fHighEnergyHits1"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 5)) / df["ahc_nHits"]
        df["fHighEnergyHits2"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 100)) / df["ahc_nHits"]
        df["fHighEnergyHits3"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 200)) / df["ahc_nHits"]

        df["meanEnergy"] = df["ahc_hitEnergy"].apply(np.mean)

        df["fHighEnergyHitsMean"] = [np.sum(df["ahc_hitEnergy"].values[i] > df["meanEnergy"].values[i]) / df["ahc_nHits"].values[i]for i in range(len(df["ahc_hitEnergy"]))]

        df["Cglobal"] = df["fHighEnergyHits1"] / df["fHighEnergyHitsMean"]       
        
        df["eDepNeutronElastic"] = df["eDepLateHits1"]-df["eDepLateHits3"]

        df["stdReco"] = df["ahc_energySum"]*0.0267     

        timeThr = 4
        #bins = [0.5, 1.5, 3, 5, 10, 30, 80, 150, 10000]
        bins = [0.5, 2.0, 10.0, 150.0, 10000.0]
        df["ahc_hitEnergyBinned"] = df["ahc_hitEnergy"].apply(lambda x: np.digitize(np.asarray(x), bins, right=True))
        timeBins = [4, 50, 10e6]
        #df["ahc_hitTime"] = df["ahc_hitTime"].apply(lambda x: np.digitize(np.asarray(x), timeBins, right=True)) 
        #df["ahc_hitRadius"] = df["ahc_hitTime"].apply(lambda x: np.asarray(x) > 45) 

        df = df.drop(columns=["ahc_hitK"])
    
    else:
        df["fLateHits1"] = df["ahc_hitTime"].apply(lambda x: np.sum(np.asarray(x) > 3)) / df["ahc_nHits"]
        df["fLateHits2"] = df["ahc_hitTime"].apply(lambda x: np.sum(np.asarray(x) > 10)) / df["ahc_nHits"]
        df["fLateHits3"] = df["ahc_hitTime"].apply(lambda x: np.sum(np.asarray(x) > 50)) / df["ahc_nHits"]
        df["fLateHits4"] = df["ahc_hitTime"].apply(lambda x: np.sum(np.asarray(x) > 75)) / df["ahc_nHits"]
        df["fLateHits10"] = df["ahc_hitTime"].apply(lambda x: np.sum(np.asarray(x) > 1000)) / df["ahc_nHits"]
        df["fLateHits11"] = df["ahc_hitTime"].apply(lambda x: np.sum(np.asarray(x) > 2000)) / df["ahc_nHits"]
        df["fLateHits12"] = df["ahc_hitTime"].apply(lambda x: np.sum(np.asarray(x) > 5000)) / df["ahc_nHits"]
        df["fLateHits13"] = df["ahc_hitTime"].apply(lambda x: np.sum(np.asarray(x) > 10000)) / df["ahc_nHits"]

        #df["fLateHitsTestbeam"] = df["fLateHits4"] - df["fLateHits11"]
        #df["fLateHitsRealistic"] = df["fLateHits1"] - df["fLateHits10"]
        
        df["eDepLateHits1"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 3) for i in range(0,len(df.index))]
        df["eDepLateHits2"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 10) for i in range(0,len(df.index))]
        df["eDepLateHits3"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 50) for i in range(0,len(df.index))]
        df["eDepLateHits4"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 75) for i in range(0,len(df.index))]
        df["eDepLateHits10"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 1000) for i in range(0,len(df.index))]
        df["eDepLateHits11"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 2000) for i in range(0,len(df.index))]
        df["eDepLateHits12"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 5000) for i in range(0,len(df.index))]
        df["eDepLateHits13"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitTime"].iloc[i] > 10000) for i in range(0,len(df.index))]
        #df["eDepLateHitsTestbeam"] = df["eDepLateHits4"] - df["eDepLateHits11"]
        #df["eDepLateHitsRealistic"] = df["eDepLateHits1"] - df["eDepLateHits10"]


        df["eDepLateHits1"] = df["eDepLateHits1"] / df["ahc_energySum"]
        df["eDepLateHits2"] = df["eDepLateHits2"] / df["ahc_energySum"]
        df["eDepLateHits3"] = df["eDepLateHits3"] / df["ahc_energySum"]
        df["eDepLateHits4"] = df["eDepLateHits4"] / df["ahc_energySum"]
        df["eDepLateHits10"] = df["eDepLateHits10"] / df["ahc_energySum"]
        df["eDepLateHits11"] = df["eDepLateHits11"] / df["ahc_energySum"]
        df["eDepLateHits12"] = df["eDepLateHits12"] / df["ahc_energySum"]
        df["eDepLateHits13"] = df["eDepLateHits13"] / df["ahc_energySum"]
        #df["eDepLateHitsTestbeam"] = df["eDepLateHitsTestbeam"] / df["ahc_energySum"]
        #df["eDepLateHitsRealistic"] = df["eDepLateHitsRealistic"] / df["ahc_energySum"]
        
    
        df["fHighEnergyHits1"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 5)) / df["ahc_nHits"]
        df["fHighEnergyHits2"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 100)) / df["ahc_nHits"]
        df["fHighEnergyHits3"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 200)) / df["ahc_nHits"]
        df["fHighEnergyHits11"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 1)) / df["ahc_nHits"]
        df["fHighEnergyHits12"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 2)) / df["ahc_nHits"]
        df["fHighEnergyHits13"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 3)) / df["ahc_nHits"]
        df["fHighEnergyHits14"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 5)) / df["ahc_nHits"]
        df["fHighEnergyHits15"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 7)) / df["ahc_nHits"]
        df["fHighEnergyHits16"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 10)) / df["ahc_nHits"]
        df["fHighEnergyHits17"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 15)) / df["ahc_nHits"]
        df["fHighEnergyHits18"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 20)) / df["ahc_nHits"]
        df["fHighEnergyHits19"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 30)) / df["ahc_nHits"]
        df["fHighEnergyHits20"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 40)) / df["ahc_nHits"]
        df["fHighEnergyHits21"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 50)) / df["ahc_nHits"]
        df["fHighEnergyHits22"] = df["ahc_hitEnergy"].apply(lambda x: np.sum(np.asarray(x) > 70)) / df["ahc_nHits"]

        df["eDepHighEnergyHits1"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitEnergy"].iloc[i] > 3) for i in range(0,len(df.index))]
        df["eDepHighEnergyHits2"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitEnergy"].iloc[i] > 5) for i in range(0,len(df.index))]
        df["eDepHighEnergyHits3"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitEnergy"].iloc[i] > 10) for i in range(0,len(df.index))]
        
        df["meanEnergy"] = df["ahc_hitEnergy"].apply(np.mean)
        df["medianEnergy"] = df["ahc_hitEnergy"].apply(np.median)
        
        df["fHighEnergyHitsMean"] = [np.sum(df["ahc_hitEnergy"].values[i] > df["meanEnergy"].values[i]) / df["ahc_nHits"].values[i]for i in range(len(df["ahc_hitEnergy"]))]
        df["fHighEnergyHitsMedian"] = [np.sum(df["ahc_hitEnergy"].values[i] > df["medianEnergy"].values[i]) / df["ahc_nHits"].values[i]for i in range(len(df["ahc_hitEnergy"]))]
        
        df["Cglobal"] = df["fHighEnergyHits1"] / df["fHighEnergyHitsMean"]       
        
        df["eDepNeutronElastic"] = df["eDepLateHits1"]-df["eDepLateHits3"]
        df["eDepNeutronElastic2"] = df["eDepLateHits1"]-df["eDepLateHits2"]

        df["femHits"] = df["ahc_hitRadius"].apply(lambda x: np.sum(np.asarray(x) < 45)) / df["ahc_nHits"]
        df["fhadrHits"] = df["ahc_hitRadius"].apply(lambda x: np.sum(np.asarray(x) > 45)) / df["ahc_nHits"]

        df["eDepEmHits"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitRadius"].iloc[i] < 45) for i in range(0,len(df.index))]
        df["eDepHadrHits"] = [np.dot(df["ahc_hitEnergy"].iloc[i],df["ahc_hitRadius"].iloc[i] > 45) for i in range(0,len(df.index))]

        df["eDepEmHits"] = df["eDepEmHits"] / df["ahc_energySum"]
        df["eDepHadrHits"] = df["eDepHadrHits"] / df["ahc_energySum"]

        df["highEnergyOuterHits1"] = [np.dot(df["ahc_hitEnergy"].iloc[i], np.logical_and(df["ahc_hitRadius"].iloc[i] > 55,df["ahc_hitEnergy"].iloc[i] > 5)) for i in range(0,len(df.index))]
        df["highEnergyOuterHits2"] = [np.dot(df["ahc_hitEnergy"].iloc[i], np.logical_and(df["ahc_hitRadius"].iloc[i] > 50,df["ahc_hitEnergy"].iloc[i] > 10)) for i in range(0,len(df.index))]
        df["highEnergyOuterHits3"] = [np.dot(df["ahc_hitEnergy"].iloc[i], np.logical_and(df["ahc_hitRadius"].iloc[i] > 45,df["ahc_hitEnergy"].iloc[i] > 50)) for i in range(0,len(df.index))]

        df["stdReco"] = df["ahc_energySum"]*0.0267     

        
        df = df.drop(columns=["ahc_hitTime"])
        df = df.drop(columns=["ahc_hitK"])
        df = df.drop(columns=["ahc_hitRadius"])
        df = df.drop(columns=["ahc_hitEnergy"])
    
    return df

def selectShowerStart(df, minShowerStart, maxShowerStart):
    df = df[df["showerStart"] >= minShowerStart]
    df = df[df["showerStart"] <= maxShowerStart]
    return df

def main():
    print('Starting makeFeatures.py ...\n')

    _hardwareConnection = utils.read_hardwareConnection("MappingIJK_mcc.txt")

    maxLayer = _nLayer
    maxHitTime = -1

    if args.maxLayer:
        maxLayer = int(args.maxLayer)
    
    if args.maxHitTime:
        maxHitTime = float(args.maxHitTime)

    if maxLayer < _nLayer:
        print("Maximum number of layers: " + maxLayer + " ("+_nLayer+")")
    if maxHitTime > 0:
        print("Removing late hit times >", maxHitTime)

    trueEnergies = []
    fileNames = []

    if args.input:
        print("\nList of input files in: ", args.input)
        with open(args.input) as f:

            for line in csv.reader(f, delimiter=','):
                trueEnergies.append(float(line[0]))
                fileNames.append(str(line[1]).strip())

        print("Reading " + str(len(fileNames)) + " input files.")
    else:
        print("No input file found. Set input file with --input.")

    #which features to read from ROOT file
    inputFeatures = ['ahc_nHits', 'ahc_energySum', 'ahc_radius', 'ahc_cogZ', 'ahc_energyPerLayer', 'ahc_nHitsPerLayer', 'ahc_hitEnergy', 'ahc_hitRadius', 'ahc_hitTime', 'ahc_hitK']
    if _convNN:
        inputFeatures = ['ahc_nHits', 'ahc_energySum', 'ahc_radius', 'ahc_cogZ', 'ahc_energyPerLayer', 'ahc_nHitsPerLayer', 'ahc_hitEnergy', 'ahc_hitRadius', 'ahc_hitTime', 'ahc_hitI', 'ahc_hitJ', 'ahc_hitK']

    for fn, E in tqdm(zip(fileNames, trueEnergies)):
        tqdm.write("\nReading energy " + str(E) + " GeV from file: " + str(fn))

        df = utils.readRootFile(fn, E, inputFeatures)

        df = shiftHitTime(df, _timeShift) # shift MC hit times

        needToRecalculate = False
        if maxHitTime > 0:
            df = removeLateHits(df, maxHitTime)
            needToRecalculate = True
        if maxLayer < _nLayer:
            df = reduceLayers(df, maxLayer)
            needToRecalculate = True
        if needToRecalculate:
            df = recalculateVariables(df)

        df = calculateShowerStart(df)
        if _localSC:
            df = selectShowerStart(df, 1, 10)

        df = calculateVariables(df) 

        #Transform layer variables to start with shower start
        df = subtractShowerStart(df)
        if not _convNN:
            df = transformLayerData(df)
            df = calculateNormCogZ(df)

        #Convert numpy arrays to string; Necessary for writing to root files
        #dfRoot = pd.DataFrame()

        #dfRoot["ahc_energyPerLayer"] = df["ahc_energyPerLayer"].apply(lambda x: np.array2string(x))
        #dfRoot["ahc_nHitsPerLayer"] = df["ahc_nHitsPerLayer"].apply(lambda x: np.array2string(x))

        #Change object datatypes to string
        #for name, column in df.items():
            #if column.dtype == object:
                #dfRoot[name] = column.apply(lambda x: np.array2string(np.array(x))).astype('S')
            #else:
                #dfRoot[name] = column


        outputPath = "./"
        addName = "_2000ns_210315_convNN"
        if args.outputPath:
            outputPath = args.outputPath
        baseName = os.path.splitext(os.path.basename(fn))[0]
        #print(baseName)
        outputFile = outputPath+baseName+addName

        #write to Root file
        if args.outputRoot:
            print("saving file to " + outputFile + ".root")
            df.to_root(outputFile+".root", key="bigtree")

        if args.outputPickle:
            print("saving file to " + outputFile + ".pickle")
            df.to_pickle(outputFile+".pickle")
        
         
        del df
        #del dfRoot

if __name__ == "__main__":
    main()