# Configuration and styles for plotting
import matplotlib.pyplot as plt
import matplotlib as mpl
import seaborn as sns

plt.rc('font', family='DejaVu Sans', size=15)
plt.rc('figure.subplot', left=0.20, bottom=0.15, right=0.95, top=0.95)
plt.rc('grid', linestyle="dotted")

color_seq = sns.cubehelix_palette(10, start=2.9, rot=0.7, dark=0.7, light=0.0, reverse=True, hue=2)
color_cat = [sns.xkcd_rgb["denim blue"], sns.xkcd_rgb["tangerine"], sns.xkcd_rgb["medium green"], sns.xkcd_rgb["pale red"], sns.xkcd_rgb["medium purple"], sns.xkcd_rgb["brownish"], sns.xkcd_rgb["light blue"], sns.xkcd_rgb["dirty yellow"], sns.xkcd_rgb["apple green"], sns.xkcd_rgb["light red"], sns.xkcd_rgb["pink"]] 
color_cat_imp = [sns.xkcd_rgb["tangerine"], sns.xkcd_rgb["medium green"], sns.xkcd_rgb["pale red"], sns.xkcd_rgb["light blue"], sns.xkcd_rgb["dirty yellow"], sns.xkcd_rgb["apple green"], sns.xkcd_rgb["light red"], sns.xkcd_rgb["pink"]] 

pltText_font = {
        'family': 'DejaVu Sans',
        'color':  'darkgray',
        'weight': 'bold',
        'style' : 'italic',
        'size': 14,
        }

pltText_font_black = {
        'family': 'DejaVu Sans',
        'color':  'black',
        'weight': 'bold',
        'style' : 'italic',
        'size': 14,
        }