# Training of models

import ROOT
from root_pandas import read_root

import numpy as np
import pandas as pd
from scipy import optimize

import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

from tqdm import tqdm
import argparse
import csv
import os
import sys
sys.path.append("/Users/christiangraf/anaconda/envs/pyRoot/lib/python3.5/site-packages/")

from math import *

import utils
from kerasNN import kerasNN
from globalSC import globalSC
from localSC import localSC
from styles import *

#pandas options
pd.options.mode.chained_assignment = None  # default='warn'

#infrastructure
parser = argparse.ArgumentParser()
parser.add_argument("--input", "-i", help="txt file with a list of input files in the format: TrueEnergy,fileName")
parser.add_argument("--outputPath", "-o", help="set the output path")
parser.add_argument("--outputPickle", "-p", help="output Pickle files?", action='store_true')
parser.add_argument("--outputRoot", "-r", help="output Root files?", action='store_true')
args = parser.parse_args()

#global variables
_nLayer = 60
_hardwareConnection = dict()

_energies = []
_fileNames = []

labelsNN = {"NN_energy": "NN energy", "NN_time": "NN time", "NN_energyTime": "NN energy&time", "NN_global": "NN global", "NN_semiGlobal_mae": "NN SG MAE", "NN_semiGlobal": "NN semi global", "NN_epl": "NN EPL", "NN_eplnpl": "NN EPL&NPL"}
labelsSC = {"SC_CGlobal": "SC CGlobal", "SC_time": "SC time", "SC_energyTime": "SC energy&time"}

#scaling features manually to zero mean and unity stdev
def scaleSingleFeature(df_train, df_test, feature, target, scaleTest=True):
    mean_ = np.mean(df_train[feature])
    stdev_ = np.std(df_train[feature])
    df_train[target] = (df_train[feature] - mean_) / stdev_
    if scaleTest:
        df_test[target] = (df_test[feature] - mean_) / stdev_

    return df_train, df_test

#scaling list of features to zero mean and unity stdev
def scaleFeatures(df_train, df_test, features):
    scaler = StandardScaler().fit(df_train[features])
    df_test[features] = scaler.transform(df_test[features])
    df_train[features] = scaler.transform(df_train[features])
    
    return df_train, df_test

def preprocessing(df_train, df_test):

    #scale features
    df_train, df_test = scaleSingleFeature(df_train, df_test, "dE", "dE_scaled", False)

    df_train, df_test = scaleSingleFeature(df_train, df_test, "stdReco", "stdReco_scaled")
    df_train, df_test = scaleSingleFeature(df_train, df_test, "beamEnergy", "beamEnergy_scaled")

    preprocessFeatures = df_train.drop(["ahc_energySum", "dE", "dE_scaled", "stdReco", "stdReco_scaled", "beamEnergy", "beamEnergy_scaled"], axis=1).columns
    df_train, df_test = scaleFeatures(df_train, df_test, preprocessFeatures)

    return df_train, df_test

def computeResolutions(df, field, fRes, fLin):
    energies = np.sort(df["beamEnergy"].unique())

    oResolutions = []
    oLinearities = []
    oEnergies = []

    for E in energies:
        myDf = df[df["beamEnergy"] == E]
        reco = myDf[field]
        linearity = fLin(reco)
        resolution = fRes(reco - E) / E

        oResolutions.append(resolution)
        oLinearities.append(linearity)
        oEnergies.append(E)

    return np.array(oResolutions), np.array(oLinearities), np.array(oEnergies)
        
def plotResolution(df, baseline, resolutions, labels, expName):
    #plot setup
    plt.rc('figure.subplot', left=0.14, bottom=0.09, right=0.98, top=0.98)
    sns.set_palette(color_cat)

    fig, axs = plt.subplots(3, 1,figsize=(5.5, 6.5), sharex=True, gridspec_kw={'height_ratios': [5, 3, 2]})
    fig.subplots_adjust(hspace=0)

    style = "-"

    energies = df["energy"]

    baseRes = df[baseline+"_res"]*100
    baseMean = df[baseline+"_mean"]

    axs[0].plot(energies, baseRes, label="Std Reco", color=color_cat[0])
    axs[2].plot(energies, (baseMean - energies)/energies*100, color=color_cat[0])

    i = 1
    for x in resolutions:
        res = df[x+"_res"]*100
        av = df[x+"_mean"]

        if x[0:2] == "SC":
            style = ":"

        axs[0].plot(energies, res, label=labels[i], color=color_cat[i])
        axs[1].plot(energies, (1-res/baseRes)*100, color=color_cat[i])
        axs[2].plot(energies, (av - energies)/energies*100, color=color_cat[i])

        i = i+1

    axs[0].set_ylim(3.5,15)
    axs[0].set_ylabel("Rel. Res. (RMS$_{90}$) [%]")
    axs[0].grid(True)

    leg = axs[0].legend(fontsize = 14)
    leg.get_frame().set_linewidth(0.0)

    axs[1].set_ylabel("Impr. [%]")
    axs[1].set_ylim(0.5,45)
    axs[1].grid(True)

    axs[2].set_ylabel("$\Delta E$ [%]")
    axs[2].set_ylim(-5.5,5.5)

    axs[2].grid(True)
    axs[2].set_xlabel("Beam Energy [GeV]")
    axs[2].set_xlim(10,85)

    figName = "plots/resolution_"+expName+".pdf"

    print("\nPlotting resolution in", figName)
    plt.savefig(figName)

def processResolutions(df, tagsSC, tagsNN, expName):

    fields = ["stdReco"]
    for tag in tagsNN:
        fields.append("reco_NN_"+tag)
    for tag in tagsSC:
        fields.append("reco_SC_"+tag)
    
    #labels = ["stdReco", "NN energy", "NN time", "NN energy&time", "NN global", "NN semi global", "NN EPL", "NN EPL&NPL"]
    myLabels = ["Std Reco"]
    for tag in tagsNN:
        if "NN_"+tag in labelsNN:
            myLabels.append(labelsNN["NN_"+tag])
        else:
            myLabels.append("NN_"+tag)
    for tag in tagsSC:
        if "SC_"+tag in labelsSC:
            myLabels.append(labelsSC["SC_"+tag])
        else:
            myLabels.append("SC_"+tag)

    df_resolution = pd.DataFrame()

    for field in fields:
        res, avg, enrg = computeResolutions(df, field, utils.std90, utils.mean90)

        df_resolution["energy"] = enrg
        df_resolution[field+"_res"] = res
        df_resolution[field+"_mean"] = avg

    plotResolution(df_resolution, baseline=fields[0], resolutions=fields[1:], labels=myLabels, expName=expName)

    resolutionFile = "results/NN_"+expName+"_res.pickle"
    print("\nWriting resolutions to pickle file:", resolutionFile)
    df_resolution.to_pickle(resolutionFile)

def runNN(df_train, df_test, features, tag, loss="mean_squared_error", arch=[128,64], drop=[0.25,0.2]):
    NN = kerasNN(features, tag=tag, epochs=50, loss=loss, arch=arch, drop=drop)
    NN.createModel()
    NN.trainModel(df_train)
    prediction = NN.testModel(df_test)

    NN.plotModelHistory()
    NN.writeModel("models/model_"+tag)

    return prediction

def handleNNs(df_train, df_test, tags, expName):

    features = dict()

    features["energy"] = ["Cglobal"]
    features["time"] = ["eDepLateHits1"]
    features["energyTime"] = ["Cglobal", "eDepLateHits1"]
    features["global"] = ["Cglobal", "eDepNeutronElastic", "eDepLateHits1", "eDepLateHits3", "normCogZ", "ahc_radius", "fHighEnergyHits1", "fHighEnergyHits2"]
    features["semiGlobal"] = ["Cglobal", "eDepNeutronElastic", "eDepLateHits1", "eDepLateHits3", "normCogZ", "ahc_radius", "fHighEnergyHits1", "fHighEnergyHits2"]
    for i in range(0,40):
        features["semiGlobal"].append("EPL_{}".format(i))
        features["semiGlobal"].append("NPL_{}".format(i))

    features["semiGlobal_mae"] = features["semiGlobal"]

    features["epl"] = []
    for i in range(0,40):
        features["epl"].append("EPL_{}".format(i))

    features["eplnpl"] = []
    for i in range(0,40):
        features["eplnpl"].append("EPL_{}".format(i))
        features["eplnpl"].append("NPL_{}".format(i))

    #features = [f_energy, f_time, f_energyTime, f_global, f_semiGlobal, f_epl, f_eplnpl]

    for tag in tags:
        print("\n--------------------")
        print(tag)
        print("--------------------\n")

        if expName == "lossfunction":
            feature = features[tag]
            print(feature)

            if tag == "semiGlobal_mae":
                prediction = runNN(df_train, df_test, feature, tag, loss="mean_absolute_error")
            else:
                prediction = runNN(df_train, df_test, feature, tag, loss="mean_squared_error")

        elif expName == "architecture":
            feature = features["semiGlobal"]
            print(feature)

            if tag == "semiGlobal":
                prediction = runNN(df_train, df_test, feature, tag)
            else:
                arch = tag.split("_")
                prediction = runNN(df_train, df_test, feature, tag, arch=arch)

        elif expName == "dropout":
            feature = features["semiGlobal"]
            print(feature)

            if tag == "semiGlobal":
                prediction = runNN(df_train, df_test, feature, tag)
            else:
                drop = tag.split("_")
                prediction = runNN(df_train, df_test, feature, tag, drop=drop)

        else:
            feature = features[tag]
            print(feature)

            prediction = runNN(df_train, df_test, feature, tag)

        print(prediction)
        df_test["prediction_NN_"+tag] = prediction
        df_test["reco_NN_"+tag] = df_test["stdReco"]/prediction
    
    return df_test


def runGlobalSC(df_train, df_test, features, tag, isEnergyDependent):
    SC = globalSC(features, isEnergyDependent)
    SC.trainModel(df_train)
    prediction = SC.testModel(df_test)

    # SC.writeModel("models/model_SC_"+tag)

    return prediction

def runLocalSC(df_train, df_test, eBins, tBins, energyAndTime, nMax, tag):

    SC = localSC(eBins, tBins, mip2GeV = 0.027)

    eHits_train, eHitsBinned_train, eHitsBinned_time_train = SC.generateHitArray(df_train, "ahc_hitEnergy", "ahc_hitTime", nMax)
    weights = SC.trainModel(df_train, eHits_train, eHitsBinned_train, eHitsBinned_time_train, energyAndTime=energyAndTime)

    eHits_test, eHitsBinned_test, eHitsBinned_time_test = SC.generateHitArray(df_test, "ahc_hitEnergy", "ahc_hitTime", nMax)
    prediction = SC.testModel(df_test, weights, eHits_test, eHitsBinned_test, eHitsBinned_time_test, energyAndTime=energyAndTime)

    # With timing
    #SC.trainModel(df_train, eHits_train, eHitsBinned_train, eHitsBinned_time_train, energyAndTime=True)

    SC.writeModel("models/model_localSC_"+tag)

    return prediction

def pipeline(df_train, df_test):

    #EXPs = ["globalSC_E", "globalSC_T", "globalSC_ET"]
    #EXPs = ["localSC_E", "localSC_ET"]
    EXPs = ["NN_semi"]
    tagsSC = []
    tagsNN = []

    for exp in EXPs:
        print("Performing experiment: " + exp + "\n")

        if exp == "globalSC_E":
            tag = "Energy"
            features = ["Cglobal"]
            prediction = runGlobalSC(df_train, df_test, features=features, tag=tag, isEnergyDependent=True)

            print(prediction)
            df_test["prediction_SC_"+tag] = prediction
            df_test["reco_SC_"+tag] = prediction
            tagsSC.append(tag)

        elif exp == "globalSC_T":
            tag = "Time"
            features = ["eDepLateHits1"]
            prediction = runGlobalSC(df_train, df_test, features=features, tag=tag, isEnergyDependent=True)

            print(prediction)
            df_test["prediction_SC_"+tag] = prediction
            df_test["reco_SC_"+tag] = prediction
            tagsSC.append(tag)

        elif exp == "globalSC_ET":
            tag = "EnergyAndTime"
            features = ["Cglobal", "eDepLateHits1"]
            prediction = runGlobalSC(df_train, df_test, features=features, tag=tag, isEnergyDependent=True)

            print(prediction)
            df_test["prediction_SC_"+tag] = prediction
            df_test["reco_SC_"+tag] = prediction
            tagsSC.append(tag)

        elif exp == "localSC_E":
            tag = "localEnergyBins" 
            # maximum number of hits. Necessary or building 2d numpy arrays
            nMax = np.max([np.max(df_train["ahc_nHits"]), np.max(df_test["ahc_nHits"])])
            # Energy and time bins
            eBins = [0.5, 1.0, 1.5, 2.5, 5.0, 15.0, 50.0, 100, 10000.0]
            tBins = [4,1e9]
            prediction = runLocalSC(df_train, df_test, eBins=eBins, tBins=tBins, energyAndTime=False, nMax=nMax, tag=tag)

            print(prediction)
            df_test["prediction_SC_"+tag] = prediction
            df_test["reco_SC_"+tag] = prediction
            tagsSC.append(tag)

        elif exp == "localSC_ET":
            tag = "localEnergyAndTimeBins" 
            # maximum number of hits. Necessary or building 2d numpy arrays
            nMax = np.max([np.max(df_train["ahc_nHits"]), np.max(df_test["ahc_nHits"])])
            # Energy and time bins
            eBins = [0.5, 1.0, 1.5, 2.5, 5.0, 15.0, 50.0, 100, 10000.0]
            tBins = [4,1e9]
            prediction = runLocalSC(df_train, df_test, eBins=eBins, tBins=tBins, energyAndTime=True, nMax=nMax, tag=tag)

            print(prediction)
            df_test["prediction_SC_"+tag] = prediction
            df_test["reco_SC_"+tag] = prediction
            tagsSC.append(tag)

        elif exp == "NN_semi":
            tagsNN.append("semiGlobal")
            df_test = handleNNs(df_train, df_test, tagsNN, expName=exp)


    
    expName = "210213_NN_semi"

    processResolutions(df_test, tagsSC=tagsSC, tagsNN=tagsNN, expName=expName)

        #df_train, df_test = preprocessing(df_train, df_test)

        #tagsNN = ["energy", "time", "energyTime", "global", "semiGlobal", "epl", "eplnpl"]
        #tagsNN = ["semiGlobal", "0_0", "10_10", "20_15", "30_20", "40_25", "50_30"]
        #df_test = handleNNs(df_train, df_test, tagsNN, expName=expName)


    return df_train, df_test

def main():
    print("Starting train model...\n")
    _hardwareConnection = utils.read_hardwareConnection("MappingIJK_mcc.txt")

    if args.input:
        print("\nList of input files in: ", args.input)
        with open(args.input) as f:

            for line in csv.reader(f, delimiter=','):
                _energies.append(float(line[0]))
                _fileNames.append(str(line[1]).strip())

        print("Reading " + str(len(_fileNames)) + " input files.")
    else:
        print("No input file found. Set input file with --input.")

    df = pd.DataFrame()

    for fn, E in tqdm(zip(_fileNames, _energies)):
        tqdm.write("\nReading energy " + str(E) + " GeV from file: " + str(fn))

        thisDf = utils.readPickleFile(fn, E)
        df = pd.concat([df, thisDf], copy=False)
        del thisDf


    df_train, df_test = train_test_split(df, test_size=0.30, random_state=42)

    df_train = utils.cutEnergyTails(df_train, mipField="ahc_energySum", beamEnergyField="beamEnergy", energyCut=0.01)

    #Mip2GeV = utils.getMip2GeV(df_train, "ahc_energySum", "beamEnergy")
    #df_train = utils.updateStdReco(df_train, Mip2GeV, "ahc_energySum", "stdReco")
    #df_test = utils.updateStdReco(df_test, Mip2GeV, "ahc_energySum", "stdReco")

    df_train["dE"] = df_train["stdReco"] / df_train["beamEnergy"] #target variable

    # The work is done here
    df_train, df_test = pipeline(df_train, df_test)

    # write test files
    for fn, E in tqdm(zip(_fileNames, _energies)):
        outputPath = "./data/60LayersScan/03trainModel/"
        addName = "_NN"
        if args.outputPath:
            outputPath = args.outputPath
        baseName = os.path.splitext(os.path.basename(fn))[0]
        outputFile = outputPath+baseName+addName

        myDf = df_test[df_test["beamEnergy"] == E]

        if args.outputRoot:
            print("saving file to " + outputFile + ".root")
            myDf.to_root(outputFile+".root", key="bigtree")

        if args.outputPickle:
            print("saving file to " + outputFile + ".pickle")
            myDf.to_pickle(outputFile+".pickle")


if __name__ == "__main__":
    main()