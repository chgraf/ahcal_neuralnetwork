import ROOT
from root_pandas import read_root

import numpy as np
import pandas as pd
import csv

from scipy import optimize

#The hardware connection is the mapping from IJK to module, chip & channel variables
def read_hardwareConnection(fn):

    hardwareConnection = dict()

    with open(fn) as tsv:

        # header
        tsv.readline()

        for line in csv.reader(tsv, delimiter=' '):
            moduleID = int(line[0])
            chipID = int(line[1])
            channel = int(line[2])

            I = int(line[3])
            J = int(line[4])
            K = int(line[5])

            hardwareConnection[(I, J, K)] = (moduleID, chipID, channel)

    print("Reading "+ str(len(hardwareConnection)) + " lines of hardware connection.\n")

    return hardwareConnection

#Reading Pickle file to DataFrame
def readPickleFile(myFile, E):
    df = pd.read_pickle(myFile)
    df["beamEnergy"] = E
    #df["EventID"] = df.index+(E-_energies[0])*len(df)
    #df.set_index("EventID")
    return df

#Reading the ROOT files to DataFrame    
#features = ['ahc_nHits', 'ahc_energySum', 'ahc_radius', 'ahc_cogZ']
def readRootFile(myFile, E, features):
    df = read_root(myFile, columns=features)
    df["beamEnergy"] = E
    return df


def stdReco(params, df, mipField="ahc_energySum", beamEnergyField="beamEnergy"):
    a = params
    #return np.sum(np.power(df["energySum"]*a - df["beamEnergy"],2))
    return np.sum(np.abs(df[mipField]*a - df[beamEnergyField])/df[beamEnergyField])

def cutEnergyTails(df, mipField="ahc_energySum", beamEnergyField="beamEnergy", energyCut=0):
    dfs = []
    i = 0
    for E in df[beamEnergyField].unique():
        myDf = df[df[beamEnergyField] == E]
        x1 = np.percentile(myDf[mipField], energyCut/2*100) 
        x2 = np.percentile(myDf[mipField], (1 - energyCut/2)*100)

        dfs.append(myDf)
        dfs[i] = myDf[myDf[mipField] > x1]
        dfs[i] = myDf[myDf[mipField] < x2]

        i+=1
    
    df = pd.concat(dfs)
    return df


# Energy cut is central interval that is cut away: energyCut=0.1 (0.05,0.95)
def getMip2GeV(df, mipField="ahc_energySum", beamEnergyField="beamEnergy", energyCut=0):
    initial_guess = [0.03]

    if energyCut > 0 and energyCut < 1:
        df = cutEnergyTails(df, mipField, beamEnergyField, energyCut)

    res = optimize.minimize(stdReco, x0=initial_guess, args=(df, mipField, beamEnergyField), method="Nelder-Mead")
    mip2GeV = res.x
        
    print("\nCalculated new MIP2GeV Calibration:", mip2GeV)

    return mip2GeV

def updateStdReco(df, mip2gev, mipField="ahc_energySum", stdRecoField="stdReco"):
    df[stdRecoField] = df[mipField]*mip2gev

    return df

#Define metrics: Calculate rms and std on central 90/95% interval
def std90(x):
    x1 = np.percentile(x, 5)
    x2 = np.percentile(x, 95)
    x = x[x > x1]
    x = x[x < x2]
    return np.std(x)

def std95(x):
    x1 = np.percentile(x, 2.5)
    x2 = np.percentile(x, 97.5)
    x = x[x > x1]
    x = x[x < x2]
    return np.std(x)

def rms90(x):
    x1 = np.percentile(x, 5)
    x2 = np.percentile(x, 95)
    x = x[x > x1]
    x = x[x < x2]
    return np.sqrt(np.power(np.std(x),2)+np.power(np.mean(x),2))

def rms95(x):
    x1 = np.percentile(x, 2.5)
    x2 = np.percentile(x, 97.5)
    x = x[x > x1]
    x = x[x < x2]
    return np.sqrt(np.power(np.std(x),2)+np.power(np.mean(x),2))

def mean90(x):
    x1 = np.percentile(x, 5)
    x2 = np.percentile(x, 95)
    x = x[x > x1]
    x = x[x < x2]
    return np.mean(x)

def mean95(x):
    x1 = np.percentile(x, 2.5)
    x2 = np.percentile(x, 97.5)
    x = x[x > x1]
    x = x[x < x2]
    return np.mean(x)